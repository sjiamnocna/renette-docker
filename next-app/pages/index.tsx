import API from '../utils/api/initapi'
import {useEffect} from "react";

export default function Home() {
  useEffect(() => {
    API;
  })
  return (
    <main id="content">
      <h1 className="text-3xl font-bold underline">
        Hello world!
      </h1>
    </main>
  )
}
