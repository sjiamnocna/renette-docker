# NextJS 13 with Renette API for Docker

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app) with Tailwind and PostCSS using next-transpile-modules for outsider modules.

Output: standalone is set for using with Docker