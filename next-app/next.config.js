// next.config.js
const withTM = require('next-transpile-modules')(['renette-api']); // pass the modules you would like to see transpiled

// parameter to use with NextJS are argument to withTM
module.exports = withTM({
  reactStrictMode: true,
  output: 'standalone',
  swcMinify: true,
  experimental: {
    externalDir: true,
  },
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: 'http://localhost:8000/api/:path*' // Proxy to Backend
      }
    ]
  },
})