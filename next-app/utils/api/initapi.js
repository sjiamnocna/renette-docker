import CAPI from "renette-api"

const API = new CAPI()

try{
    API.authenticateWithName('renette')
    API.authorizeWithKey('12345')
} catch (e){
    console.log(e)
}

export default API