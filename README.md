# Dockerized NextJS + Nette minimal API
Effective combination of [Nette Framework](https://nette.org/) and [NextJS](https://nextjs.org/) or [ReactJS](https://reactjs.org/) app in [Docker](https://www.docker.com/) container.

## What's inside
- Development *docker-compose_dev.yml* and production *docker-compose_prod.yml* setup for developing apps using NextJS and Nette Framework with MySQL
- Nginx configuration for running in development machine
- Simple PHP API with [Nette minimal](https://github.com/sjiamnocna/nette-minimal.git)
- NextJS application with [Renette-api](https://github.com/sjiamnocna/renette-api)

## How to make it work?
- Clone
- Run `composer install` in `/phpcode/` and `yarn` in `nextapp` directory
- Run `docker-compose -f docker-compose_dev.yml up --build` for development (with nginx server and volumes)
- Run `docker-compose -f docker-compose_prod.yml up --build` for production (Without volumes, with multi-stage build for better container performance)
  - In case of production the Nginx is not present, copy the [Nginx config file](docker/nginx/conf.d/app.conf) to your `nginx/sites-available/` and configure docker containers ports to `upstream`
  - You will probably want to introduce a server name (for domain)
  - You will probably want setup `HTTPS` after that, e.g. with `certbot`
- If using Nette, `touch phpcode/log phpcode/temp`

### Using BIN/runner.php
- Allows to run commands using CMD
- Run in Docker container as `docker exec -it container_xyz php vendor/bin/runcommand.php CTransactionLoader`

### Permission denied problem
If you bump into `Permission denied` problem during build, play around with permissions and ownership on `/docker/.data/` and `/docker/.log/`

My advice to fix this is running:
```shell
sudo chmod 775 -R docker/.log docker/.data & \
sudo chown -R $USER:$USER docker/.log docker/.data
```

This is Docker's evergreen. Always permission problems :)

Add following to your `~/.bashrc` or `~/.zshrc` if you use ZSH to define `UID` - userID and `GID` groupID
```shell
export UID=$(id -u)
export GID=$(id -g)
```

Make sure the directory permissions for `/docker/(.log|.data)/` dir are as follows
```shell
drwxr-xr-x 2  $USER root       4096 srp 20 11:44 phpfpm
```

## Ports
- **Configure the firewall to not expose these ports to public,
use SSH tunnel for accessing ports on the server and Proxy pass in your webserver**

- **Change ports if needed (eg. another apps running on your server need it)**
- Ports exposed to host machine are
  - 9906 for MySQL
  - 3000 for Node app (Proxy pass)
  - 9000 for PHP-FPM app (FastCGI pass)
  - 8000 for Nginx server (removed in production)

## Logs and (temp) data
- All data are volumes located in `./docker` directory, so you don't have to worry about it, you'll find it all at once there

## Fork, contribute, submit issue, play with it
Happy hacking :)